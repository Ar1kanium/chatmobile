import "react-native-gesture-handler";
import { registerRootComponent } from "expo";
import App from "./App";
import OneSignal from "react-native-onesignal";
import { OS_APP_ID } from "./consts";

OneSignal.setLogLevel(6, 0);
OneSignal.setAppId(OS_APP_ID);

// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in Expo Go or in a native build,
// the environment is set up appropriately
registerRootComponent(App);
