import * as React from "react";
import AppealPage from "./components/AppealPage";
import { SafeAreaProvider } from "react-native-safe-area-context";
import ChatPage from "./components/ChatPage";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Pending from "./components/Pending";
import CameraView from "./components/CameraView";

const Stack = createStackNavigator();

export default function App() {
  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Appeal" headerMode="none">
          <Stack.Screen name="Appeal" component={AppealPage} />
          <Stack.Screen name="Pending" component={Pending} />
          <Stack.Screen name="Chat" component={ChatPage} />
          <Stack.Screen name="Camera" component={CameraView} />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
}
