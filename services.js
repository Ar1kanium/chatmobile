import OneSignal from "react-native-onesignal";

export function sendNotification(userId) {
  const data = {
    include_player_ids: [userId],
    contents: { en: "Оператор вошёл в диалог" },
  };
  const jsonString = JSON.stringify(data);
  OneSignal.postNotification(jsonString);
}
