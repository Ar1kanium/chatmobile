import * as React from "react";
import {
  Dimensions,
  Keyboard,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import {
  BottomSheet,
  Button,
  Icon,
  ListItem,
  Input,
} from "react-native-elements";
import database from "@react-native-firebase/database";
import inAppMessaging from "@react-native-firebase/in-app-messaging";

const AppealPage = ({ navigation }) => {
  const [data, setData] = React.useState(null);
  const [loading, setLoading] = React.useState(true);
  const [name, setName] = React.useState("");
  const [theme, setTheme] = React.useState("Выбрать тему");
  const [subTheme, setSubTheme] = React.useState("Выбрать подтему");
  const [initialMess, setInitialMess] = React.useState("");
  const [isVisible, setIsVisible] = React.useState({
    theme: false,
    subTheme: false,
  });

  const openThemeModal = () => {
    setIsVisible({
      ...isVisible,
      theme: true,
    });
  };
  const openSubThemeModal = () => {
    setIsVisible({
      ...isVisible,
      subTheme: true,
    });
  };
  const closeModal = () => {
    setIsVisible({
      theme: false,
      subTheme: false,
    });
  };

  const createDialogue = () => {
    const newRef = database().ref("/pending").push();
    newRef
      .set({
        clientname: name,
        topic: subTheme,
        timestamp: { ".sv": "timestamp" },
        messages: {
          initialMessage: {
            author: "client",
            message: initialMess,
            timestamp: { ".sv": "timestamp" },
          },
        },
      })
      .then(() => {
        setName("");
        setTheme("Выбрать тему");
        setSubTheme("Выбрать подтему");
        setInitialMess("");
        navigation.navigate("Pending", {
          dialogueId: newRef.key,
        });
      });
  };

  React.useEffect(() => {
    const startApp = async () => {
      await inAppMessaging().setMessagesDisplaySuppressed(true);
      await database()
        .ref("/topics")
        .once("value")
        .then((snap) => {
          setData(snap.val());
          setLoading(false);
        });
    };
    startApp();
  }, []);

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
        <View style={styles.bigCircle} />
        <View style={styles.smallCircle} />
        <View style={styles.centerizedView}>
          <View style={styles.identityBox}>
            <View style={styles.logoBox}>
              <Icon
                color="#fff"
                name="comments"
                type="font-awesome"
                size={50}
              />
            </View>
            <Text style={styles.loginTitleText}>Ar1Chat</Text>
            <Button
              onPress={openThemeModal}
              buttonStyle={styles.button}
              title={theme}
              titleStyle={styles.buttonText}
              loading={loading}
            />
            <Button
              buttonStyle={styles.button}
              title={subTheme}
              titleStyle={styles.buttonText}
              onPress={openSubThemeModal}
              disabled={theme === "Выбрать тему"}
            />
            <Input
              placeholder="Ваше имя"
              style={styles.input}
              value={name}
              onChangeText={(e) => setName(e)}
            />
            <Input
              placeholder="Здравствуйте, мне очень хочется кодить, но у меня лапки, что делать???"
              style={styles.inputProblem}
              value={initialMess}
              onChangeText={(e) => setInitialMess(e)}
              multiline={true}
              numberOfLines={3}
              label="Опишите Вашу проблему"
            />
            <Button
              buttonStyle={styles.button}
              title="Войти в чат"
              onPress={createDialogue}
              disabled={subTheme === "Выбрать подтему" || !name || !initialMess}
              titleStyle={styles.buttonText}
            />
          </View>
        </View>
        <BottomSheet
          isVisible={isVisible.theme}
          modalProps={{ onRequestClose: closeModal }}
          containerStyle={{ backgroundColor: "rgba(0.5, 0.25, 0, 0.2)" }}
        >
          {loading ? (
            <View></View>
          ) : (
            Object.keys(data).map((text, i) => (
              <ListItem
                key={i}
                onPress={() => {
                  if (theme !== text) {
                    setTheme(text);
                    setSubTheme("Выбрать подтему");
                  }
                  closeModal();
                }}
              >
                <ListItem.Content>
                  <ListItem.Title>{text}</ListItem.Title>
                </ListItem.Content>
              </ListItem>
            ))
          )}
        </BottomSheet>
        <BottomSheet
          isVisible={isVisible.subTheme}
          containerStyle={{ backgroundColor: "rgba(0.5, 0.25, 0, 0.2)" }}
        >
          {theme === "Выбрать тему" ? (
            <View></View>
          ) : (
            data[theme].map((text, i) => (
              <ListItem
                key={i}
                onPress={() => {
                  setSubTheme(text);
                  closeModal();
                }}
              >
                <ListItem.Content>
                  <ListItem.Title>{text}</ListItem.Title>
                </ListItem.Content>
              </ListItem>
            ))
          )}
        </BottomSheet>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: "relative",
  },
  bigCircle: {
    width: Dimensions.get("window").height * 0.7,
    height: Dimensions.get("window").height * 0.7,
    backgroundColor: "#3896ff",
    borderRadius: 1000,
    position: "absolute",
    right: Dimensions.get("window").width * 0.25,
    top: -50,
  },
  smallCircle: {
    width: Dimensions.get("window").height * 0.4,
    height: Dimensions.get("window").height * 0.4,
    backgroundColor: "#3896ff",
    borderRadius: 1000,
    position: "absolute",
    bottom: Dimensions.get("window").width * -0.2,
    right: Dimensions.get("window").width * -0.3,
  },
  centerizedView: {
    width: "100%",
    top: "15%",
  },
  identityBox: {
    width: "80%",
    backgroundColor: "#fafafa",
    borderRadius: 20,
    alignSelf: "center",
    paddingHorizontal: 14,
    paddingBottom: 30,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  logoBox: {
    width: 100,
    height: 100,
    backgroundColor: "#327aff",
    borderRadius: 1000,
    alignSelf: "center",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    top: -50,
    marginBottom: -50,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  loginTitleText: {
    fontSize: 26,
    fontWeight: "bold",
    alignSelf: "center",
    marginTop: 10,
    marginBottom: 10,
  },
  input: {
    width: "100%",
    marginTop: 10,
  },
  inputProblem: {
    textAlignVertical: "top",
    borderRadius: 4,
  },
  button: {
    backgroundColor: "#327aff",
    marginTop: 10,
    paddingVertical: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold",
  },
  disabledButton: {
    backgroundColor: "#3896ff",
    marginTop: 10,
    paddingVertical: 10,
    borderRadius: 4,
  },
  disabledButtonText: {
    color: "#fff",
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold",
  },
});

export default AppealPage;
