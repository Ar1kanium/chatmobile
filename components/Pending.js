import * as React from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  Keyboard,
  BackHandler,
} from "react-native";
import database from "@react-native-firebase/database";
import { Icon, Text, CheckBox } from "react-native-elements";
import OneSignal from "react-native-onesignal";
import { sendNotification } from "../services";
import { useFocusEffect } from "@react-navigation/native";

const Pending = ({ navigation, route }) => {
  const { dialogueId } = route.params;
  const [data, setData] = React.useState(null);
  const [queue, setQueue] = React.useState(null);
  const [osUserId, setOSUserId] = React.useState(null);
  const [notificationToggle, setNotificationToggle] = React.useState(true);
  const prevDataRef = React.useRef();

  React.useEffect(() => {
    const ref = database().ref(`/pending`);
    const listener = ref.on("value", (snap) => setData(snap.val()));
    const detectUserId = async () => {
      const deviceState = await OneSignal.getDeviceState();
      setOSUserId(deviceState.userId);
    };
    detectUserId();
    return () => {
      ref.off("value", listener);
    };
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        return true;
      };

      BackHandler.addEventListener("hardwareBackPress", onBackPress);

      return () =>
        BackHandler.removeEventListener("hardwareBackPress", onBackPress);
    }, [])
  );

  React.useEffect(() => {
    prevDataRef.current = data;
  });
  const prevData = prevDataRef.current;

  React.useEffect(() => {
    const checkUpdates = () => {
      if (data && data.hasOwnProperty(dialogueId)) {
        if (data[dialogueId].hasOwnProperty("operator")) {
          const operatorId = data[dialogueId]["operator"];
          database()
            .ref(`/pending/${dialogueId}`)
            .remove()
            .then(
              setTimeout(
                () =>
                  navigation.navigate("Chat", {
                    operatorId,
                    dialogueId,
                  }),
                0
              )
            )
            .then(() => {
              if (notificationToggle) sendNotification(osUserId);
            });
        } else {
          if (!prevData) {
            setQueue(Object.keys(data).length - 1);
          } else {
            let counter = 1;
            Object.keys(data).forEach((el) => {
              if (
                data[el].hasOwnProperty("timestamp") &&
                data[el]["timestamp"] < data[dialogueId]["timestamp"]
              )
                counter++;
            });
            setQueue(counter);
          }
        }
      }
    };
    checkUpdates();
  }, [data]);

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
        <View style={styles.bigCircle} />
        <View style={styles.smallCircle} />
        <View style={styles.centerizedView}>
          <View style={styles.identityBox}>
            <View style={styles.logoBox}>
              <Icon
                color="#fff"
                name="comments"
                type="font-awesome"
                size={50}
              />
            </View>
            <Text style={styles.loginTitleText}>Ar1Chat</Text>
            <Text style={styles.text} h4>
              {" "}
              Место в очереди: {queue}{" "}
            </Text>
            <CheckBox
              title="Прислать уведомление, когда оператор войдёт в диалог"
              checked={notificationToggle}
              onPress={() => setNotificationToggle(!notificationToggle)}
            />
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: "relative",
  },
  bigCircle: {
    width: Dimensions.get("window").height * 0.7,
    height: Dimensions.get("window").height * 0.7,
    backgroundColor: "#3896ff",
    borderRadius: 1000,
    position: "absolute",
    right: Dimensions.get("window").width * 0.25,
    top: -50,
  },
  smallCircle: {
    width: Dimensions.get("window").height * 0.4,
    height: Dimensions.get("window").height * 0.4,
    backgroundColor: "#3896ff",
    borderRadius: 1000,
    position: "absolute",
    bottom: Dimensions.get("window").width * -0.2,
    right: Dimensions.get("window").width * -0.3,
  },
  centerizedView: {
    width: "100%",
    top: "15%",
  },
  identityBox: {
    width: "80%",
    backgroundColor: "#fafafa",
    borderRadius: 20,
    alignSelf: "center",
    paddingHorizontal: 14,
    paddingBottom: 30,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  logoBox: {
    width: 100,
    height: 100,
    backgroundColor: "#327aff",
    borderRadius: 1000,
    alignSelf: "center",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    top: -50,
    marginBottom: -50,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  loginTitleText: {
    fontSize: 26,
    fontWeight: "bold",
    alignSelf: "center",
    marginTop: 10,
    marginBottom: 10,
    paddingBottom: 10,
  },
  text: {
    paddingBottom: 10,
    alignSelf: "center",
  },
  input: {
    width: "100%",
    marginTop: 10,
  },
  inputProblem: {
    textAlignVertical: "top",
    borderRadius: 4,
  },
  button: {
    backgroundColor: "#327aff",
    marginTop: 10,
    paddingVertical: 10,
    borderRadius: 4,
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold",
  },
  disabledButton: {
    backgroundColor: "#3896ff",
    marginTop: 10,
    paddingVertical: 10,
    borderRadius: 4,
  },
  disabledButtonText: {
    color: "#fff",
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold",
  },
});

export default Pending;
