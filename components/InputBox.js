import * as React from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faPaperPlane, faCamera } from "@fortawesome/free-solid-svg-icons";
import database from "@react-native-firebase/database";
import { usePubNub } from "pubnub-react";
import { useNavigation } from "@react-navigation/native";

const InputBox = ({ operatorId, dialogueId }) => {
  const [inputState, setInputState] = React.useState("");
  const [typingStatus, setTypingStatus] = React.useState(false);
  const pubnub = usePubNub();
  const navigation = useNavigation();

  React.useEffect(() => {
    if (inputState && !typingStatus) {
      setTypingStatus(true);
      pubnub.signal({ message: "true", channel: `${dialogueId}client` });
    }
    if (!inputState && typingStatus) {
      setTypingStatus(false);
      pubnub.signal({ message: "false", channel: `${dialogueId}client` });
    }
  }, [inputState, pubnub, typingStatus, dialogueId]);

  async function sendMessage() {
    if (inputState) {
      const newMes = database()
        .ref(`/active/${operatorId}/${dialogueId}/messages`)
        .push();
      await newMes.set({
        timestamp: { ".sv": "timestamp" },
        author: "client",
        message: inputState,
      });
      await database()
        .ref(`/active/${operatorId}/${dialogueId}`)
        .update({
          timestamp: { ".sv": "timestamp" },
        });
      setInputState("");
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.mainContainer}>
        <TextInput
          style={styles.textInput}
          value={inputState}
          onChangeText={setInputState}
          multiline
        />
        {inputState === "" && (
          <TouchableOpacity
            onPress={() =>
              navigation.navigate("Camera", {
                dialogueId,
                operatorId,
              })
            }
          >
            <FontAwesomeIcon
              size={24}
              color="#b9b9b9"
              icon={faCamera}
              style={styles.icon}
            />
          </TouchableOpacity>
        )}
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity onPress={sendMessage}>
          <FontAwesomeIcon icon={faPaperPlane} color="white" size={24} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    margin: 10,
    alignItems: "flex-end",
  },
  textInput: {
    flex: 1,
    marginHorizontal: 5,
  },
  mainContainer: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "white",
    borderRadius: 20,
    marginRight: 10,
    alignItems: "flex-end",
  },
  buttonContainer: {
    backgroundColor: "#327aff",
    borderRadius: 50,
    width: 50,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
  },
  icon: {
    marginHorizontal: 10,
    marginBottom: 12,
  },
  emoji: {
    position: "absolute",
    width: "100%",
    height: "50%",
  },
});

export default InputBox;
