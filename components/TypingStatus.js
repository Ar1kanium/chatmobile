import * as React from "react";
import { usePubNub } from "pubnub-react";
import { StyleSheet, View } from "react-native";

const TypingStatus = ({ dialogueId }) => {
  const [typing, setTyping] = React.useState(false);
  const pubnub = usePubNub();
  const handleSignal = (e) => {
    const msg = e.message;
    if (msg === "true") setTyping(true);
    if (msg === "false") setTyping(false);
  };
  React.useEffect(() => {
    pubnub.addListener({ signal: handleSignal });
    pubnub.subscribe({ channels: [`${dialogueId}operator`] });
  }, [pubnub, dialogueId]);

  return (
    <>
      {typing && (
        <View style={styles.wrapper}>
          <View style={styles.dot} />
          <View style={styles.dot} />
          <View style={styles.dot} />
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    borderRadius: 25,
    backgroundColor: "#e5e5e5",
    justifyContent: "space-evenly",
    alignItems: "center",
    flexDirection: "row",
    height: 30,
    width: 100,
    marginLeft: 10,
  },
  dot: {
    backgroundColor: "#b9b9b9",
    width: 10,
    height: 10,
    borderRadius: 50,
  },
});

export default TypingStatus;
