import * as React from "react";
import {
  ActivityIndicator,
  BackHandler,
  FlatList,
  Keyboard,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import database from "@react-native-firebase/database";
import { Avatar, Text, Icon, Input, Overlay } from "react-native-elements";
import ChatMessage from "./ChatMessage";
import { useFocusEffect } from "@react-navigation/native";
import InputBox from "./InputBox";
import { PubNubProvider } from "pubnub-react";
import Pubnub from "pubnub";
import TypingStatus from "./TypingStatus";
import FinishDialogueButton from "./FinishDialogueButton";

const ChatPage = ({ navigation, route }) => {
  const { operatorId, dialogueId } = route.params;
  const [operatorInfo, setOperatorInfo] = React.useState(null);
  const [messagesData, setMessagesData] = React.useState(null);
  const pubnub = new Pubnub({
    publishKey: "pub-c-b22a2df5-165e-4f95-a3b4-55933c5b8b15",
    subscribeKey: "sub-c-c79d9adc-d2cc-11eb-9280-fa77d5b6609d",
    uuid: dialogueId,
  });
  // uuid shouldn't change for device, implemented here that way, cause app developed for learning purposes

  React.useEffect(() => {
    const loadOperatorData = () => {
      return database()
        .ref(`/users/${operatorId}`)
        .once("value")
        .then((snap) => {
          setOperatorInfo(snap.val());
        });
    };
    loadOperatorData();
    const ref = database().ref(`/active/${operatorId}/${dialogueId}/messages`);
    const listener = ref.on("value", (snap) => setMessagesData(snap.val()));
    return () => ref.off("value", listener);
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        return true;
      };

      BackHandler.addEventListener("hardwareBackPress", onBackPress);

      return () =>
        BackHandler.removeEventListener("hardwareBackPress", onBackPress);
    }, [])
  );

  const checkAvatar = () => {
    return (
      operatorInfo &&
      operatorInfo.hasOwnProperty("avatar") &&
      operatorInfo["avatar"]
    );
  };
  const checkName = () => {
    return (
      operatorInfo &&
      operatorInfo.hasOwnProperty("name") &&
      operatorInfo["name"]
    );
  };
  const goToMainPage = () => {
    navigation.navigate("Appeal");
  };
  const avatarSource = checkAvatar()
    ? { uri: operatorInfo["avatar"] }
    : {
        uri: "https://cdn.iconscout.com/icon/free/png-256/operator-1411794-1194283.png",
      };

  const messagesArr = messagesData
    ? Object.values(messagesData).sort((a, b) => {
        return b["timestamp"] > a["timestamp"];
      })
    : null;

  if (!messagesData || !operatorInfo) {
    return <ActivityIndicator />;
  }

  return (
    <PubNubProvider client={pubnub}>
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.avatar}>
            <Avatar rounded size="medium" source={avatarSource} />
          </View>
          <Text style={styles.header__text} h4>
            {" "}
            {checkName() ? `Оператор ${operatorInfo["name"]}` : "Оператор"}{" "}
          </Text>
          <View style={styles.icon}>
            <FinishDialogueButton
              operatorId={operatorId}
              dialogueId={dialogueId}
            />
          </View>
        </View>
        <View style={styles.container}>
          <FlatList
            data={messagesArr}
            renderItem={({ item }) => (
              <ChatMessage
                message={item}
                avatar={avatarSource}
                start={messagesData["fOpMes"]["timestamp"]}
                lastMessTime={messagesArr[0]["timestamp"]}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            inverted
          />
        </View>
        <TypingStatus dialogueId={dialogueId} />
        <InputBox operatorId={operatorId} dialogueId={dialogueId} />
      </View>
    </PubNubProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "white",
    flexDirection: "row",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
  avatar: {
    marginLeft: 10,
  },
  icon: {
    marginRight: 5,
  },
  header__text: {
    color: "black",
    alignSelf: "center",
  },
  content: {
    flex: 1,
  },
  input: {
    flexDirection: "row",
    backgroundColor: "#ebebeb",
    alignItems: "center",
  },
});

export default ChatPage;
