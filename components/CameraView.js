import * as React from "react";
import { RNCamera } from "react-native-camera";
import { Text, TouchableOpacity, View, StyleSheet } from "react-native";
import { PureComponent } from "react";
import database from "@react-native-firebase/database";
import { faCamera } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";

class CameraView extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <RNCamera
          style={{ flex: 1, alignItems: "center" }}
          ref={(ref) => {
            this.camera = ref;
          }}
          captureAudio={false}
        />
        <View
          style={{ flex: 0, flexDirection: "row", justifyContent: "center" }}
        >
          <TouchableOpacity
            onPress={this.takePicture.bind(this)}
            style={styles.capture}
          >
            <FontAwesomeIcon size={24} color="white" icon={faCamera} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  takePicture = async () => {
    if (this.camera) {
      const { navigation, route } = this.props;
      const { operatorId, dialogueId } = route.params;
      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options);
      const newMes = database()
        .ref(`/active/${operatorId}/${dialogueId}/messages`)
        .push();
      await newMes.set({
        timestamp: { ".sv": "timestamp" },
        image: true,
        author: "client",
        message: `data:image/jpeg;base64,${data.base64}`,
      });
      await database()
        .ref(`/active/${operatorId}/${dialogueId}`)
        .update({
          timestamp: { ".sv": "timestamp" },
        });
      await navigation.goBack();
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "black",
  },
  capture: {
    flex: 0,
    backgroundColor: "#327aff",
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: "center",
    margin: 20,
  },
});

export default CameraView;
