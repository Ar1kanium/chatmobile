import * as React from "react";
import { Text, View, StyleSheet, Image, Animated } from "react-native";
import { Avatar } from "react-native-elements";
import moment from "moment";
import "moment/locale/ru";

const ChatMessage = ({ message, avatar, start, lastMessTime }) => {
  const isMyMessage = () => {
    return message["author"] === "client";
  };
  const timeDisplay = (unitTime) => {
    const now = moment();
    const messageTime = moment(unitTime);
    const timediff = now.diff(messageTime);
    return timediff > 21600000
      ? moment(unitTime).locale("ru").calendar({ sameElse: "LLL" })
      : moment(unitTime - 10000)
          .locale("ru")
          .fromNow();
  };
  const animationVal = new Animated.Value(0);
  const ifLastMessage = () => lastMessTime === message["timestamp"];
  const itemStyle = ifLastMessage()
    ? [
        styles.container,
        { opacity: animationVal },
        {
          transform: [
            { scale: animationVal },
            {
              rotate: animationVal.interpolate({
                inputRange: [0, 1],
                outputRange: isMyMessage()
                  ? ["35deg", "0deg"]
                  : ["-35deg", "0deg"],
                extrapolate: "clamp",
              }),
            },
            { perspective: 1000 },
          ],
        },
      ]
    : styles.container;

  React.useEffect(() => {
    const animate = () => {
      Animated.timing(animationVal, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }).start();
    };
    animate();
  });

  return (
    <Animated.View style={itemStyle}>
      {start === message["timestamp"] && (
        <View style={styles.startDialogue}>
          <Text>
            Диалог начат{" "}
            {moment(start)
              .locale("ru")
              .calendar({ sameElse: "LLL" })
              .toLowerCase()}
          </Text>
        </View>
      )}
      <View style={styles.messageBox}>
        {!isMyMessage() && (
          <View style={styles.avatar}>
            <Avatar rounded size="medium" source={avatar} />
          </View>
        )}
        <View
          style={[
            styles.content,
            ,
            {
              backgroundColor: isMyMessage() ? "#327aff" : "#e5e5e5",
              marginLeft: isMyMessage() ? 50 : 0,
              marginRight: isMyMessage() ? 0 : 50,
              borderBottomLeftRadius: isMyMessage() ? 5 : 0,
              borderBottomRightRadius: isMyMessage() ? 0 : 5,
            },
          ]}
        >
          {message.hasOwnProperty("image") ? (
            <Image source={{ uri: message["message"] }} style={styles.image} />
          ) : (
            <Text
              style={[
                styles.message,
                {
                  color: isMyMessage() ? "white" : "black",
                },
              ]}
            >
              {message["message"]}
            </Text>
          )}
          <Text
            style={[
              styles.time,
              {
                color: isMyMessage() ? "white" : "black",
              },
            ]}
          >
            {timeDisplay(+message["timestamp"])}
          </Text>
        </View>
      </View>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  messageBox: {
    flexDirection: "row",
  },
  content: {
    flex: 1,
    justifyContent: "space-between",
    borderRadius: 5,
    padding: 10,
  },
  avatar: {
    marginRight: 10,
    alignSelf: "flex-end",
  },
  message: {},
  startDialogue: {
    alignSelf: "center",
    backgroundColor: "white",
    borderRadius: 20,
    marginRight: 10,
    padding: 10,
    marginBottom: 10,
    marginTop: -10,
  },
  time: {
    alignSelf: "flex-end",
    fontSize: 10,
  },
  image: {
    height: 200,
    width: 230,
    borderRadius: 5,
    alignSelf: "center",
  },
});

export default ChatMessage;
