import * as React from "react";
import { Icon, Overlay, Rating } from "react-native-elements";
import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import {
  faGrimace,
  faTired,
  faGrinTongueSquint,
  faGrin,
  faGrinStars,
} from "@fortawesome/free-solid-svg-icons";
import database from "@react-native-firebase/database";
import { useNavigation } from "@react-navigation/native";

const FinishDialogueButton = ({ operatorId, dialogueId }) => {
  const [overlayVisibility, setOverlayVisibility] = React.useState(false);
  const [rating, setRating] = React.useState(null);
  const iconType = [
    faGrimace,
    faTired,
    faGrinTongueSquint,
    faGrin,
    faGrinStars,
  ];
  const navigation = useNavigation();
  async function finishDialogue() {
    await database().ref(`/active/${operatorId}/${dialogueId}`).update({
      rating: rating,
    });
    await navigation.navigate("Appeal");
  }

  return (
    <View>
      <Icon
        name="checkmark-done-outline"
        color="#327aff"
        type="ionicon"
        onPress={() => setOverlayVisibility(true)}
        reverse
      />
      <Overlay
        isVisible={overlayVisibility}
        onBackdropPress={() => setOverlayVisibility(false)}
      >
        <View style={styles.overlay}>
          <Text style={styles.text}> Пожалуйста, поставьте оценку! </Text>
          <Rating
            minValue={1}
            onFinishRating={(r) => setRating(r)}
            startingValue={rating ? rating : 2.5}
          />
          <TouchableOpacity onPress={finishDialogue}>
            {rating && (
              <View style={styles.btn}>
                <FontAwesomeIcon
                  icon={iconType[rating - 1]}
                  size={26}
                  style={styles.icon}
                />
                <Text style={styles.btn__text}> Завершить диалог </Text>
                <FontAwesomeIcon
                  icon={iconType[rating - 1]}
                  size={26}
                  style={styles.icon}
                />
              </View>
            )}
          </TouchableOpacity>
        </View>
      </Overlay>
    </View>
  );
};

const styles = StyleSheet.create({
  overlay: {
    alignItems: "center",
    flexDirection: "column",
  },
  text: {
    fontSize: 24,
    marginBottom: 20,
  },
  btn: {
    backgroundColor: "#327aff",
    marginTop: 50,
    padding: 10,
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 5,
  },
  icon: {
    color: "white",
  },
  btn__text: {
    color: "white",
    fontSize: 20,
    marginHorizontal: 10,
  },
});

export default FinishDialogueButton;
